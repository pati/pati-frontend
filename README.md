## Install

Let's go!

```
$ yarn // or npm install
```

## Development

Run development page on **localhost:8080**

```
$ yarn run start
```

## Build

Build for production.

```
$ yarn run build
```


